#kmeans

#data(wine, package="rattle")
library(NbClust)


wssplot <- function(data, nc=20, seed=1234){
    wss <- (nrow(data)-1)*sum(apply(data,2,var))
    for (i in 2:nc){
        set.seed(seed)
        wss[i] <- sum(kmeans(data, centers=i)$withinss)}
    plot(1:nc, wss, type="b", xlab="Number of Clusters",
         ylab="Within groups sum of squares")}


df=scale(usersForCluster[-1])
sum(usersForCluster$scaledContributions)
wssplot(df) 
nc <- NbClust(df, min.nc=2, max.nc=5, method="kmeans")
table(nc$Best.n[1,])

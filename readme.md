# Description
I am analyzing the social dynamics of participation in a genealogical community, collaborating on a one-world family tree. This tree is contains 16,599,248 person-profiles, added by 501,728 individuals. Each unique individual is expected to have a single profile, and most of the profiles are conected to the main tree (3,501,852 are unconnected), necessitating a high degree of coordination and cooperation. 

Almost all profiles, except for living, and recently living, people are 'open' (i.e. editable by anyone). Participation and coordination occurs both at the level of profile creation and editing, and discussion in the 'G2G' question and answer forum. (There are additional levels of project coordination, data-management, and curatorial 'challenges' I won't consider here).

## I am considering two kinds of user data. 

### G2G q\&a section participation data

In this part of the community, users pose questions, and others answer. A large portion of the questions are asked by new users, for basic website or methodological assistance, but other questions are asked by experienced users about higher level issues. Responses can be answers, or comments on the question or answers. 

Questions, and answers, have upvotes and downvotes. All users have profiles where the number of questions, answers and comments are displayed, along with upvotes/downvotes given and received.

G2G metrics assayed were:

1. Number of questions asked, answered, and commented on.
2. Number of upvotes/downvotes given and received.
3. Length of texts.
    * number of characters.
4. Sentiment of texts
    * a simple positive or negative score, based on word sentiment polarity, ranging between -1:1
5. Complexity of texts.
    * number of syllables per word, divided by the number of words in a text.
6. Number of questions asked/answered.

Ultimately, user-level summaries of these metrics were normalised by active period length where appropriate, and weighted by text-length (where appropriate). For the most part log transformed data, or log-ratios were assessed (e.g. log((asks+1)/(answered+1)). Almost all metrics were highly right-skewed.

A large number of posters are either singletons (asking or answering only one question), or no longer active. Out of 14844 unique users, only 3286 were active posting questions/answers in the last 150 days. For some users, participation is short, on the order of days, weeks, or months. In order to assess the duration of engagement, date quartiles of active participation in G2G were assessed as early, mid and late (days before present). All count measures were scaled by the late-early interquartile range, as a proxy for active period length.

To restrict analyses to users with an extended active period, an arbitrary interquartile range >log(2)=7.4 days was chosen. There were 4835 users that fit this criterion.

### The actual genealogical tree section

Note: it is entirely possible to be an active user in this genealogical community (adding new profiles, or editing existing profiles) without participating in the g2g section. There is no single metric to assess 'optimal participation'.

1. Number of contributions (profiles created or edited)
2. Number of thanks received (for profiles created or edited)

As above, these data were scaled by active-period, and log transformed.


## A priori interpretation of the data (domains of interest)

### Persistence

*   earlyVsLate, length of the interquartile participation duration, number of contributions

### Sociability
*   number of g2g entries, meanTextLength (per text)

### Positivity
*   positive sentiment of text, thanks received, ratio of negative to positive votes given, ratio of negative to positive votes gotten.

### Service
*    votes given vs gotten, asks vs Answers

### Smarts
*   text complexity (syllables per word).

# I have found

*   Trait dimension is relatively low (btw, this surprised me).
    * cluster analysis (AgglomerativeClustering in Python, NbClust in R) suggests a relatively low number of effective dimensions of variation (2-3).
    * PC analysis basically agrees (4-5 dimensions, acc. to scree plot).
*   Positivity generalizes (positive sentiment positively correlates with thanks received, and the ratio of upvotes given or received). 
*   Positivity correlates strongly with participaton in disussions (numbers of texts), weakly with productivity (tree contributions), and not at all with questions asked vs answered.
*   Answer complexity (smarts) doesn't correlate with much of anything.
*   'Service' seems to be a valid dimension. Users who score high on the relative number of votes they give, also score relatively high on the number of questions they answer, the length of their responses, and the amount they participate (and, independently, the thanks they get).

## response = number of questions, answers, comments:

``Response: scaledWrits``
``                    Sum Sq   Df  F value    Pr(>F)    ``  
``meanTextLength        0.05    1   0.4134  0.520285    ``  
``textComplexity        0.01    1   0.1179  0.731326    ``  
``logSentRatio          6.05    1  53.3241 3.291e-13 ***``  
``asksVsAnswers        69.54    1 613.1159 < 2.2e-16 ***``  
``giveVsGet             1.01    1   8.9154  0.002842 ** ``  
``scaledThanks         89.09    1 785.4335 < 2.2e-16 ***``  
``scaledContributions   0.52    1   4.6231  0.031593 *  ``  

## response = thanks received:

``Response: scaledThanks``
``                     Sum Sq   Df   F value    Pr(>F)    ``  
``meanTextLength         0.36    1    1.7112    0.1909    ``  
``textComplexity         0.01    1    0.0650    0.7987    ``  
``logSentRatio           0.21    1    1.0001    0.3173    ``  
``asksVsAnswers          0.05    1    0.2392    0.6248    ``  
``giveVsGet              6.26    1   29.4536 6.007e-08 ***``  
``scaledWrits          166.89    1  785.4335 < 2.2e-16 ***``  
``scaledContributions  725.19    1 3412.8633 < 2.2e-16 ***``  

## response = positive affect

``Response: logSentRatio``  
``                     Sum Sq   Df F value    Pr(>F)    ``  
``meanTextLength        0.224    1 10.0223  0.001556 ** ``  
``textComplexity        0.133    1  5.9336  0.014891 *  ``  
``scaledThanks          0.022    1  1.0001  0.317339    ``  
``asksVsAnswers         0.169    1  7.5664  0.005969 ** ``  
``giveVsGet             0.037    1  1.6327  0.201390    ``  
``scaledWrits           1.194    1 53.3241 3.291e-13 ***``  
``scaledContributions   0.004    1  0.1694  0.680698    ``  

## response = complexity

``Response: textComplexity``  
``                     Sum Sq   Df F value    Pr(>F)    ``  
``meanTextLength      0.00024    1  1.3346   0.24805    ``  
``logSentRatio        0.00105    1  5.9336   0.01489 *  ``  
``scaledThanks        0.00001    1  0.0650   0.79871    ``  
``asksVsAnswers       0.01009    1 57.0133 5.145e-14 ***``  
``giveVsGet           0.00031    1  1.7365   0.18765    ``  
``scaledWrits         0.00002    1  0.1179   0.73133    ``  
``scaledContributions 0.00031    1  1.7377   0.18750    ``  


## Initial Variables
* giveUp = number of other questions/answered upvoted
* giveDown = number of other questions/answered downvoted
* contribs = number of profile edits made
* nAnswers = number of questions answered
* nComments = number of questions/answers commented on
* nVotes = number of questions voted on
* years = number of years active
* weightMean = the weighted mean sentiment of the comments
* nConts = number of contributions the sentiment analysis was conducted on
* nPosts = number of questions made
* meanLen = average utterance length
* getUp/getDown = average 

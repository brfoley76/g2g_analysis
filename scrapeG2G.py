import urllib2
from bs4 import BeautifulSoup
from textblob import TextBlob
import re
import json

class G2GAnalysis():
    def __init__(self, url):
        self.home=url
        self.path=url[:(url.find('questions'))]
        self.page = urllib2.urlopen(self.home)
        self.Users={} #dict holding the users
        self.Questions={} # dict holding the questions
        self.flipped=0 # number of questions flipped through
            
    def runScrape(self, nQuestions):
        '''
        Run through a bunch of pages of g2g questions
        Parse them for the users, the users comments
        and the user stats.
        Fill a dictionary of results.
        '''
        
        tick=0
        self.soup = BeautifulSoup(self.page, 'html.parser')
        while self.flipped < nQuestions:
            tick+=1
            for title in self.soup.findAll('div', attrs={'class': 'qa-q-item-title' }):                
                self.flipped+=1
                link=title.find('a').get('href')
                if self.checkQuestion(link):
                    self.scrapeQuestion(link)
            startTick=tick*50
            print tick
            with open('data.txt', 'w') as outfile:
                json.dump(self.Users, outfile)
            nextUrl=self.home+'?start='+str(startTick)
            self.page = urllib2.urlopen(nextUrl)
            self.soup=BeautifulSoup(self.page, 'html.parser')
                
    def checkQuestion(self, link):
        '''
        make sure I haven't queried this question before
        if not, 
        add the question to the dictionary of questions
        '''
        questNumber=link.split('/')[1]
        if not questNumber in self.Questions:
            self.Questions[questNumber]=self.path+link[1:]
            questPage=urllib2.urlopen(self.path+link[1:])
            return True
        else:
            return False
    
    def scrapeQuestion(self, link):
        '''
        Go through the page
        1.0: the question (only one)
        1.1: the comments on the question (0 to many) 
        2.0: the answers (0 to many)
        2.1: the comments on each answer in turn (0 to many)
        '''
        link=self.path+link[1:]
        #print link
        page = urllib2.urlopen(link)
        qPage=BeautifulSoup(page, 'html.parser')
        qTitle=qPage.find('title').text
        qTitle=qTitle[:(qTitle.find(' - WikiTree G2G'))]
        question= qPage.find('div', attrs={'class': 'qa-part-q-view' })
        try:
            questionText=question.find('div', attrs={'class': 'entry-content' }).text
        except:
            questionText=''
        questionText=qTitle+questionText
        try:
            whoQ=question.find('span', attrs={'class': 'qa-q-view-who-data' })
            whoQID=whoQ.find('a', attrs={'class': 'qa-user-link' }).get('href')
        
            self.addUserAndText(whoQID,questionText)
        except:
            pass
        
        # comments on the original question
        for qc in qPage.findAll('div', attrs={'class': 'qa-c-list-item hentry comment' }):
            try:
                qcID=qc.find('a', attrs={'class' : 'qa-user-link'}).get('href')
                qcText=qc.find('div', attrs={'class': 'entry-content' }).text
                self.addUserAndText(qcID,qcText)
            except:
                pass

        for answer in qPage.findAll('div', attrs={'class':'qa-a-list-item  hentry answer'}):
            try:
                answerText=answer.find('div', attrs={'class':'qa-a-item-content'}).text
                answerID=answer.find('a', attrs={'class':'qa-user-link'}).get('href')
                self.addUserAndText(answerID,answerText)
            except:
                pass
            for comment in answer.findAll('div', attrs={'class':'qa-a-item-c-list'}):
                ac=comment.find('div', attrs={'class':'qa-c-item-content'})
                try:
                    acText=ac.text
                    acID=ac.find('a',  attrs={'class':'qa-user-link'} ).get('href')
                    self.addUserAndText(acID,acText)
                except:
                    pass     
    
    def clean_text(self, text):
        '''
        Utility function to clean tweet text by removing links, special characters
        using simple regex statements.
        '''
        cleanText=' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", text).split())
        return cleanText
        
    def get_text_sentiment(self, text):
        '''
        Utility function to classify sentiment of passed tweet
        using textblob's sentiment method
        '''
        text=self.clean_text(text)
        # create TextBlob object of passed tweet text
        analysis = TextBlob(text)
        # set sentiment
        #print analysis.sentiment.polarity
        return analysis.sentiment.polarity
        #if analysis.sentiment.polarity > 0:
        #    return 1
        #elif analysis.sentiment.polarity == 0:
        #    return 0
        #else:
        #    return -1

    def addUserAndText(self, uID,uText):
        userID=uID.split('/user/')[1]
        if not userID in self.Users:
            self.addUser(userID)
        textVal=self.get_text_sentiment(uText)
        self.Users[userID]['textN']+=1 #another measure of their text
        self.Users[userID]['textSum']+=textVal
        
    def addUser(self, userID):
        userPath=self.path+'user/'+userID
        userpage = urllib2.urlopen(userPath)
        userSoup=BeautifulSoup(userpage, 'html.parser')
        userDets={}        

        userDets['nPosts'] = userSoup.find('span', attrs={'class' : "qa-uf-user-q-posts"}).text
        userDets['nAnswers']=userSoup.find('span', attrs={'class' : "qa-uf-user-a-posts"}).text
        userDets['nComments'] = userSoup.find('span', attrs={'class' : "qa-uf-user-c-posts"}).text
        userDets['nVotes'] = userSoup.find('span', attrs={'class' : "qa-uf-user-q-votes"}).text
        userDets['giveUp'] = userSoup.find('span', attrs={'class' : "qa-uf-user-upvotes"}).text #gave
        userDets['giveDown'] = userSoup.find('span', attrs={'class' : "qa-uf-user-downvotes"}).text
        userDets['getUp'] = userSoup.find('span', attrs={'class' : "qa-uf-user-upvoteds"}).text #received
        userDets['getDown'] = userSoup.find('span', attrs={'class' : "qa-uf-user-downvoteds"}).text

        userURL='https://www.wikitree.com/wiki/'+userID
        userpage= urllib2.urlopen(userURL)
        userSoup=BeautifulSoup(userpage, 'html.parser')

        tick=0
        for small in userSoup.findAll('div', attrs={'class' : "SMALL"}):
            if tick==1:
                #print userID
                details = small.text.split('|')
                #print details
                years=details[0].strip()[-5:]
                userDets['years']=2018-int(years)
                contribs=details[1].split(' ')[2]
                userDets['contribs']=int(contribs)
            tick+=1
        userDets['textN']=0
        userDets['textSum']=0
        self.Users[userID]=userDets

    
    def queryPageParser(self, page):
        pass
    
aaa=G2GAnalysis("https://www.wikitree.com/g2g/questions")
aaa.runScrape(6000)

setwd("/home/brad/Documents/machine_learning/data_incubator/finalChallenge/images")
list.files()
userDat=read.csv('../userData2.csv', as.is=T)
head(userDat)
library(IDPmisc)

## giveUp = number of other questions/answered upvoted
## giveDown = number of other questions/answered downvoted
## contribs = number of profile edits made
## nAnswers = number of questions answered
## nComments = number of questions/answers commented on
## nVotes = I think number of questions voted on
## years = number of years active
## weightMean = the weighted mean sentiment of the comments
## nConts = number of contributions the sentiment analysis was conducted on
## nPosts = number of questions made
## meanLen = average utterance length
## getUp/getDown = average 

### Transform data ##########
userDat[,'giveUp']=as.numeric(gsub(',','',userDat[,'giveUp']))
userDat[,'giveDown']=as.numeric(gsub(',','',userDat[,'giveDown']))
userDat[,'nAnswers']=as.numeric(gsub(',','',userDat[,'nAnswers']))
userDat[,'nPosts']=as.numeric(gsub(',','',userDat[,'nPosts']))
userDat[,'nComments']=as.numeric(gsub(',','',userDat[,'nComments']))
userDat[,'getUp']=as.numeric(gsub(',','',userDat[,'getUp']))

# how positive vs negative 
userDat$negGet=log((userDat$getDown+1)/(userDat$getUp+1))
userDat$negGive=log((userDat$giveDown+1)/(userDat$giveUp+1))
## userDat$logSentRatio

# how demanding vs giving
userDat$asksVsAnswers=log((userDat$nPosts+1)/(userDat$nAnswers+userDat$nComments+1))
userDat$giveVotes=userDat$giveUp+userDat$giveDown
userDat$getVotes=userDat$getUp+userDat$getDown
userDat$giveVsGet=log((userDat$giveVotes+1)/(userDat$getVotes+1))

# how productive and consistent
earlyEffort=userDat$textEarly-userDat$textMid
earlyEffort[which(earlyEffort<0)]=0
laterEffort=userDat$textMid-userDat$textLate
laterEffort[which(laterEffort<0)]=0

lengthEffort=userDat$textEarly-userDat$textLate
lengthEffort[which(lengthEffort<0)]=0
lengthEffort=lengthEffort+1
userDat$lengthEffort=lengthEffort
userDat$earlyVsLate=log((earlyEffort+1)/(laterEffort+1))
userDat$g2gWrits=userDat$nPosts+userDat$nAnswers+userDat$nComments
userDat$scaledWrits=log(userDat$g2gWrits/userDat$lengthEffort+1)
userDat$scaledNTexts=log(userDat$nTexts/userDat$lengthEffort+1)
userDat$scaledThanks=log(userDat$thanks/userDat$lengthEffort +1)
userDat$scaledContributions=log((userDat$contributions+1)/userDat$lengthEffort)

userDat$lengthEffort=log(lengthEffort)

### userDat$nTexts

### How sophisticated
#userDat$textComplexity
userDat$meanTextLength=log(userDat$meanTextLength+1)


#X giveUp textEarly nTexts textMid nAnswers
#nComments giveDown meanTextLength logSentRatio textComplexity textLate thanks
#year nPosts getDown contributions day getUp mon    negGet    negGive
#asksVsAnswers giveVotes getVotes  giveVsGet lengthEffort earlyVsLate g2gWrits
#scaledWrits scaledNTexts scaledThanks scaledContributions

columnOrder=c('X','active', 'year', 'giveUp', 'giveDown', 'getUp', 'getDown','giveVotes','getVotes',
              'nPosts','nComments','nAnswers','nTexts','contributions','g2gWrits',
              'textEarly','textMid','########## Blurb#########
              # got more data. 
              ###  14844 users. 
              ###  65829 questions
              ###  479511 pieces of text.textLate',
              'negGet','negGive','thanks',
              'meanTextLength','textComplexity','logSentRatio',
              'asksVsAnswers','giveVsGet','lengthEffort','earlyVsLate',
              'scaledWrits', 'scaledNTexts', 'scaledThanks', 'scaledContributions')

analyseColumns=c('meanTextLength','textComplexity','logSentRatio',
                 'asksVsAnswers','giveVsGet','lengthEffort','earlyVsLate',
                 'scaledWrits', 'scaledNTexts', 'scaledThanks', 'scaledContributions')

par(mfrow=c(2,2))
subUsers=userDat[which(userDat$lengthEffort>2),]
subUsersRelevant=subUsers[,analyseColumns]


plot(subUsers$scaledContributions~subUsers$asksVsAnswers)

usersForCluster=subUsers[,analyseColumns]
write.csv(usersForCluster,'regularUsers.csv')


userPC=princomp(subUsers[,analyseColumns], cor=T)
userPC$loadings
userPC$sdev

plot(userPC$scores[,1]~userPC$scores[,3])
plot(userPC$sdev)

aaa=lm(scaledWrits~meanTextLength+textComplexity+logSentRatio+
           asksVsAnswers+giveVsGet+scaledThanks+
           scaledContributions, data=subUsers)

bbb=lm(scaledThanks~meanTextLength+textComplexity+logSentRatio+
           asksVsAnswers+giveVsGet+scaledWrits+
           scaledContributions, data=subUsers)

ccc=lm(logSentRatio~meanTextLength+textComplexity+scaledThanks+
           asksVsAnswers+giveVsGet+scaledWrits+
           scaledContributions, data=subUsers)

plot(scaledWrits~scaledThanks, data=subUsers)
plot(scaledThanks~scaledContributions, data=subUsers)
plot(logSentRatio~scaledWrits, data=subUsers)



